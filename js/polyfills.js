Set.prototype.difference = function (setB) {
    let _difference = new Set(this);
    for (let elem of setB) {
        _difference.delete(elem);
    }
    return _difference;
};

Array.prototype.contains = function (test) {
    return this.indexOf(test) !== -1;
};

Array.prototype.remove = function (item) {
    let indexOf = this.indexOf(item);
    if(indexOf>-1){
        this.splice(indexOf, 1);
    }
    return this;
};

Array.prototype.equals = function (b) {
    if (this.length !== b.length) {
        return false;
    }

    let a = [...this].sort();
    b.sort();

    let i = 0;
    while (i < a.length) {
        if (a[i] !== b[i]) {
            return false;
        }
        ++i;

    }
    return true;
};

