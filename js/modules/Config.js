import {Logger} from "./Logger.js";

const c = {
    DELAY: 100,
    FULL_SET: () => [1, 2, 3, 4, 5, 6, 7, 8, 9],
    logger: new Logger(true, false),
    COMPUTED_COLOR: "#90EE90",
    CHANGED_COLOR: "#FFFF00",
    UNCHANGED_COLOR: "#FFA500"
}

export const Config = c;