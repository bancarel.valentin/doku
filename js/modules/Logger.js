export class Logger {
    constructor(DEBUG, TRACE) {
        this.allowDebug = DEBUG;
        this.allowTrace = TRACE;
    }


    debug() {
        if (this.allowDebug && this.allowTrace) {
            console.debug(...arguments);
        }
    }

    info() {
        if (this.allowDebug) {
            console.info(...arguments);
        }
    }


    warn() {
        if (this.allowDebug) {
            console.warn(...arguments);
        }
    }

    error() {
        console.error(...arguments);
    }

    // noinspection JSUnusedGlobalSymbols
    trace() {
        console.trace(...arguments);
    }
}