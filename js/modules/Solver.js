import {Config} from './Config.js'
import {Box} from './Box.js'
import {Util} from "./Util.js";


export class Solver {

    grid = [];
    _solved = false;
    _soluce = null;
    _iterators = null;
    _lastMessage = null;


    constructor() {
    }

    load(def) {
        if (def && def.length !== 81) {
            throw "Not a good length";
        }
        for (let r = 1; r <= 9; r++) {
            this.grid[r] = this.grid[r] || [undefined];
            for (let c = 1; c <= 9; c++) {
                this.grid[r][c] = new Box(r, c, def[Util.flatIndex(r, c)]);
            }
        }
    }

    get solved() {
        return this._solved;
    }

    get soluce() {
        if (!this._soluce) {
            throw "Not resolved yet";
        }
        return this._soluce;
    }

    get iterators() {
        return this._iterators;
    }

    get lastMessage() {
        return this._lastMessage;
    }

    getDefaultStatus() {
        return {computed: false, changed: false}
    };

    async solve() {
        this._solve(true)
    }


    /**
     * Solve the grid passed in the constructor parameter.
     *
     * The solving is done by computing all possibles values in all grid box.
     * If one box can only have one value then we set it's final value and don't look at it again.
     * Once every boxes have been computed, if the grid changed during this run, we start over. Otherwise we are stuck.
     *
     * @returns {string|null|*}
     */
    async _solve(veryFirst) {
        this.start = this.start || new Date().getTime();
        this._iterators = this._iterators || {"global": 0};
        this._iterators.global++;
        let broken = false;

        await this.onIterationStart();
        this._solved = true;
        main: for (let methodName of this.getAllSolvers()) {
            let realMethodNAme = "solve_" + methodName;
            this._iterators[methodName] = this._iterators[methodName] || {call: 0, changed: 0, computed: 0};
            this._iterators[methodName].call++;

            for (let r = 1; r <= 9; r++) {
                for (let c = 1; c <= 9; c++) {
                    let box = this.grid[r][c];
                    if (box.getFinalValue() === null) {
                        await this.onComputeBoxStart(r, c);

                        /**
                         * Call one sudoku technique
                         * Return a status object having
                         * changed being true if the cell pencil marks or final value changed during this try, false otherwise
                         * computed being true if the cell final value is now solved, false otherwise
                         */
                        let innerStatus = this[realMethodNAme](r, c) || this.getDefaultStatus();
                        if (innerStatus.changed === true) this._iterators[methodName].changed++;
                        if (innerStatus.computed === true) this._iterators[methodName].computed++;


                        await this.onComputeBoxEnd(r, c, innerStatus);

                        // State changed, we need to start from scratch (unless first run and first method; to set first pencil marks)
                        if (veryFirst === false && (innerStatus.changed || innerStatus.computed)) {
                            broken = true;
                            break main;
                        }
                        this._solved = false;// Box was not final and is not computed.
                    }
                }
            }

            // On first method execution during first iteration we did not break to set all pencil marks.
            // Because of that we want to start again but as normal this one
            if (veryFirst === true) {
                broken = true;
                break;
            }
        }
        await this.onIterationFinish();

        if (broken) {
            await this._solve(false);
        } else if (this._solved) {
            this.validate(true);
            this._soluce = this.toString(true, true);
            this.__lastMessage = `Solved!`;
        } else if (broken === false) {
            this.__lastMessage = `Stuck after ${this._iterators.global} iteration. Not powerful enough.`;
        }
        await this.notify();
        await this.onCompletion();
    }

    getAllSolvers() {
        let results = [];

        function properties(obj) {
            // https://codereview.stackexchange.com/questions/30702/function-to-get-all-properties-of-an-object
            let props, i;
            if (obj == null) {
                return results;
            }
            if (typeof obj !== 'object' && typeof obj !== 'function') {
                return properties(obj.constructor.prototype);
            }
            props = Object.getOwnPropertyNames(obj);
            i = props.length;
            while (i--) {
                if (!~results.indexOf(props[i])) {
                    results.push(props[i]);
                }
            }
            return properties(Object.getPrototypeOf(obj));
        }

        return properties(this)
            .filter(methodName => methodName.startsWith("solve_"))
            .map(methodName => methodName.substring(6))
            .sort((a, b) => {
                let _a = parseInt(a.substring(0, 1));
                let _b = parseInt(b.substring(0, 1));
                return _a - _b;
            });
    }

    // noinspection JSUnusedGlobalSymbols
    solve_1_normalRules(r, c) {
        let blocked = [...this.getRow(r), ...this.getColumn(c), ...this.getInnerBox(r, c)]
            .map(box => box.getFinalValue() || null)
            .filter(val => val && !isNaN(val));
        return this.removePossibilities(r, c, blocked, "They are a final value in a blocking row, column or square.");
    }

    // noinspection JSUnusedGlobalSymbols
    solve_2_lastPossibleInSubSet(r, c) {
        let thisPossibles = this.grid[r][c].getPossibleValues();
        let sets = [this.getRow(r), this.getColumn(c), this.getInnerBox(r, c)];

        for (let oneThisPossible of thisPossibles) {
            itSets: for (let set of sets) {
                for (let box of set) {
                    if (!box.is(r, c) && (box.getFinalValue() === oneThisPossible || box.getPossibleValues().contains(oneThisPossible))) {
                        // Not unique possible in set, go to next set.
                        continue itSets;
                    }
                }
                // No other box in the subset can hold oneThisPossible; so we can set it as final here.
                this.removePossibilities(r, c, Config.FULL_SET().remove(oneThisPossible), `${oneThisPossible} can only be in this place in the current subset`);
                return;
            }
        }
    }

    // noinspection JSUnusedGlobalSymbols
    solve_3_blockedByALineOfPossible(r, c) {
        let currentPossibleValues = this.grid[r][c].getPossibleValues();
        let blocked = [];
        itAllPossible: for (let pos of currentPossibleValues) {// We are checking every value possible in one box we will call B.
            itRowSquares:for (let tmpC = 1; tmpC <= 9; tmpC += 3) {// For all inner boxes on B's row
                if (this.isInInnerBox(r, c, r, tmpC)) {
                    continue;
                }
                let innerBox = this.getInnerBox(r, tmpC);
                let innerBoxF = innerBox.map(box => box.getFinalValue() || null).filter(val => val && !isNaN(val));
                if (innerBoxF.contains(pos)) {
                    continue;
                }
                for (let t of innerBox) {
                    if (t.getRow() !== r && t.getPossibleValues().contains(pos)) {
                        // B value can be on another line in another inner-box.
                        // So this inner-box does not help ruling the value out. We go to the next inner-box.
                        continue itRowSquares;
                    }
                }
                // B value can not be on another row than B's row in this inner-box.
                // So in fact B is not possible in another place in this row, we can rule it out where we thought it might go.
                blocked.push(pos);
                continue itAllPossible;
            }
            if (!blocked.contains(pos)) {
                // We try the same via the columns in case it didn't worked.
                itColSquares:for (let tmpR = 1; tmpR <= 9; tmpR += 3) {// For all inner boxes on this column
                    if (this.isInInnerBox(r, c, tmpR, c)) {
                        continue;
                    }
                    let innerBox = this.getInnerBox(tmpR, c);
                    let innerBoxF = innerBox.map(box => box.getFinalValue() || null).filter(val => val && !isNaN(val));
                    if (innerBoxF.contains(pos)) {
                        continue;
                    }
                    for (let t of innerBox) {
                        if (t.getColumn() !== c && t.getPossibleValues().contains(pos)) {
                            continue itColSquares;
                        }
                    }
                    blocked.push(pos);
                    continue itAllPossible;
                }
            }
        }
        return this.removePossibilities(r, c, blocked, "They must be in a blocking row or column in another square ");
    }

    // noinspection JSUnusedGlobalSymbols
    solve_4_CouplesOrTrouples(r, c) {
        let blocked = [];
        let sets = [this.getRow(r), this.getColumn(c), this.getInnerBox(r, c)];
        for (let set of sets) {
            let thisSetPossibilitiesCount = {};
            for (let box of set) {
                if (!box.is(r, c)) {// Do not check myself
                    let flatPencilMarks = box.possibleValues.sort().join();
                    thisSetPossibilitiesCount["_" + flatPencilMarks] = thisSetPossibilitiesCount["_" + flatPencilMarks]++ || 1;

                    if (thisSetPossibilitiesCount["_" + flatPencilMarks] === flatPencilMarks.length) {
                        for (let nePossibleCount of thisSetPossibilitiesCount) {
                            // Only if digit in pair/trouple are nowhere else in set
                        }
                        blocked.push(box.possibleValues);
                    }
                }
            }
        }
        this.removePossibilities(r, c, blocked, "They are par of a blocking pair or trio on this line or column");
    }

    /**
     * Remove digits from one box possibilities
     * @param r row index
     * @param c column index
     * @param blocked digits to remove
     * @param reason
     */
    removePossibilities(r, c, blocked, reason) {
        Config.logger.debug(`Removing ${blocked} from ${r},${c} because '${reason}'`);
        let status = this.getDefaultStatus();
        const final = this.grid[r][c].getFinalValue();
        if (final === null) {
            blocked = new Set([...blocked]);
            let currentPossibleValues = new Set([...this.grid[r][c].getPossibleValues()]);

            let newPossibles = Array.from(currentPossibleValues.difference(blocked));
            if (newPossibles.length === 0) {
                Config.logger.error("Smells like shit.");
                debugger;
                throw "Implementation error";
            }
            if (newPossibles.length === 1) {
                status.computed = true;
                status.changed = true;
                this.grid[r][c].setFinalValue(newPossibles[0]);
                console.trace(`${r},${c} is now set to ${newPossibles[0]}`);
            } else if (!this.grid[r][c].getPossibleValues().equals(newPossibles)) {
                status.changed = true;
                this.grid[r][c].setPossibleValues(newPossibles);
                console.trace(`${r},${c}'s possibles is now set to ${newPossibles}`);
            } else {
                console.trace(`${r},${c} not changed`);
            }
        }
        return status;
    }

    /**
     * Return all the Boxes in the row at the index given in parameter
     * @param r the row index you wish to retrieve
     * @returns Box[] the list of box in this row
     */
    getRow(r) {
        let result = [];
        for (let c = 1; c <= 9; c++) {
            result.push(this.grid[r][c]);
        }
        return result;
    }

    /**
     * Return all the Boxes in the col at the index given in parameter
     * @param c the col index you wish to retrieve
     * @returns Box[] the list of box in this col
     */
    getColumn(c) {
        let result = [];
        for (let r = 1; r <= 9; r++) {
            result.push(this.grid[r][c]);
        }
        return result;
    }

    /**
     * Return all the Boxes in the inner square holding the coordinate given as parameters
     * @param wantedR one row index in the square you want to retrieve
     * @param wantedC one col index in the square you want to retrieve
     * @returns Box[] the list of box in this square
     */
    getInnerBox(wantedR, wantedC) {
        let {minR, maxR, minC, maxC} = this.getInnerBoxLimit(wantedR, wantedC);

        const result = [];
        for (let r = 1; r <= 9; r++) {
            for (let c = 1; c <= 9; c++) {
                if (r <= maxR && r >= minR && c <= maxC && c >= minC) {
                    result.push(this.grid[r][c])
                }
            }
        }
        return result;
    }

    isInInnerBox(r, c, wantedR, wantedC) {
        let {minR, maxR, minC, maxC} = this.getInnerBoxLimit(wantedR, wantedC);
        return r <= maxR & r >= minR && c <= maxC && c >= minC;
    }

    getInnerBoxLimit(wantedR, wantedC) {
        let minR = (Math.floor((wantedR - 1) / 3) * 3) + 1;
        let maxR = minR + 2;
        let minC = (Math.floor((wantedC - 1) / 3) * 3) + 1;
        let maxC = minC + 2;
        return {minR, maxR, minC, maxC};
    }

    /**
     * Validate every and every column to ensure they all have 9 distinct values.
     */
    validate(isFinal) {
        for (let r = 1; r <= 9; r++) {
            let arr = [...this.getRow(r).map(b => b.getFinalValue()).filter(v => v !== null)];
            let set = new Set(arr);
            if (arr.length !== set.size) {
                debugger;
                Config.logger.error("Same value in row");
            }
            if (isFinal === true && set.size !== 9) {
                return false;
            }
        }

        for (let c = 1; c <= 9; c++) {
            let arr = [...this.getColumn(c).map(b => b.getFinalValue()).filter(v => v !== null)];
            let set = new Set(arr);
            if (arr.length !== set.size) {
                debugger;
                Config.logger.error("Same value in column");
            }
            if (isFinal === true && set.size !== 9) {
                return false;
            }
        }


        for (let r = 1; r <= 9; r += 3) {
            for (let c = 1; c <= 9; c += 3) {
                let arr = [...this.getInnerBox(r, c).map(b => b.getFinalValue()).filter(v => v !== null)];
                let set = new Set(arr);
                if (arr.length !== set.size) {
                    debugger;
                    throw "ERROR";
                }
                if (isFinal === true && set.size !== 9) {
                    Config.logger.error("Same value in square");
                }
            }

        }
        return true;
    }


    /**
     * Return the current grid as more human readable format
     * @param flattened
     * @param asFinales define the return format you wish.
     *      If false, each cell in the grid will hold an array of its possible values;
     *      if true, each cell in the grid will hold its current final value.
     * @returns [[]] A 2D array of arrays, int, or a mix depending of the given parameter.
     */
    toString(flattened = true, asFinales = true) {
        let result = flattened ? "" : [];
        for (let r = 1; r <= 9; r++) {
            if (!flattened) {
                result[r] = [];
            }
            for (let c = 1; c <= 9; c++) {
                if (flattened) {
                    result += (this.grid[r][c].getFinalValue() || "0")
                } else if (asFinales === true) {
                    result[r][c] = this.grid[r][c].getFinalValue()
                } else {
                    result[r][c] = this.grid[r][c].getPossibleValues()
                }

            }
        }
        return result;
    }

    async onComputeBoxStart(r, c) {
    }

    async onComputeBoxEnd(r, c, status) {
    }

    async onIterationFinish() {
    }

    async onIterationStart() {
    }

    async onCompletion() {
    }

    async notify() {
        Config.logger.debug(this.__lastMessage);
        Config.logger.debug(this._iterators);
        Config.logger.debug(this.toString());
    }

}