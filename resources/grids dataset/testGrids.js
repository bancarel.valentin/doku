// noinspection JSUnusedGlobalSymbols
export const grids = [
    {
        "grid": "897426315615973284400085976734519628186207539952638147548392761379861452261754893",
        "notes": "Dummy grid",
        "source": null,
        "soluce": "897426315615973284423185976734519628186247539952638147548392761379861452261754893"
    },
    {
        "grid": "897400005010003200023100076704509020180000039050608107540002760009800050200004893",
        "notes": "Most simple grid, resolvable via normal sudoku rules only",
        "source": null,
        "soluce": "897426315615973284423185976734519628186247539952638147548392761379861452261754893"
    },
    {
        "grid": "000000004000002758049003000600050020000320000001000080500080900014000002000400007",
        "notes": "Resolvable by checking that one digit pencil mark is in only one box of one of it's subsets",
        "source": "https://www.sudoku9x9.com/expert.php?529241171",
        "soluce": null
    },
    {
        "grid": "300008109010905070200001000732000060000000000060000792000400007040806030903700008",
        "notes": "Resolvable by checking if possibility are blocked in the same row/col in another square",
        "source": "https://www.mots-croises.ch/Sudoku/grille.htm?g=G114933348751-ea92c&t=N4",
        "soluce": "375648129416925873289371456732189564594267381861534792628453917147896235953712648"
    }
];
